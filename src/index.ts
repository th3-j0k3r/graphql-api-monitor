import { argv } from 'process';

import { getSchema } from './schema';
import { parseConfig, fillWithDefaultValues } from './config';
import { diff, commitChanges, ensureRepositoryExists } from './git';
import { notify } from './webhooks/discord';

// Sketchy argument parsing because I don't want to bring in a huge dependency only for one argument.
if (argv[argv.length - 2] != '--config') {
    console.error(`Missing required --config CONFIG_FILE_PATH argument`);
    process.exit(1);
}

const configPath = argv[argv.length - 1];
const config = parseConfig(configPath);

(async () => {
    if (!config.targets) {
        console.error('No targets were defined in the configuration');
        return;
    }

    for (const target of config.targets.map(fillWithDefaultValues)) {
        console.log(`Checking for updates to the ${target.name} API.`);

        const schema = await getSchema(target);
        if (schema == null) {
            console.error(`Could not fetch GraphQL schema for ${target.name}.`);
            continue;
        }

        const root = config.dataDirectory || '.';
        const newRepository = ensureRepositoryExists(root, target.name);

        const diffOutput = diff(schema, root, target.name);
        if (diffOutput == null) {
            if (newRepository) {
                console.log(`First run for ${target.name}.`);
            } else {
                console.log(`GraphQL API for ${target.name} did not change.`);
                continue;
            }
        } else {
            console.log(diffOutput);
            if (config.webhook) {
                notify(diffOutput, config.webhook, target.name);
            }
        }

        commitChanges(root, target.name);
    }
})();